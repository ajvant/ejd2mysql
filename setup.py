from setuptools import setup, find_packages

deps = ['pymysql', 'pyyaml', 'hiyapyco']
ideps = ['setuptools-scm>=3.3.0']

setup(
    name='ejd2mysql',
    packages=['ejd2mysql'],
    package_dir={'ejd2mysql': 'src'},
    include_package_data=True,
    install_requires=deps,
    setup_requires=ideps,
    description='ejabberd external auth script for mysql db',
    zip_safe=False,
    entry_points={'console_scripts': ['ejd2mysql = ejd2mysql.main:main']},
    use_scm_version={'write_to': 'src/version.py'},
)
