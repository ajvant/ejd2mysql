#!/usr/bin/python3

""" Provides external auth of ejd against MySQL databases

See also: https://docs.ejabberd.im/developer/guide/#external
"""

import sys
import struct
import logging
import logging.config
import hashlib
from argparse import ArgumentParser
from os.path import realpath, dirname
from os.path import join as pathjoin
from types import SimpleNamespace
from functools import partial

import hiyapyco
import pymysql
from pymysql.cursors import DictCursor

from .version import version


log = logging.getLogger(__name__)


def load_config(path):
    """ Load configuration """
    here = dirname(realpath(__file__))
    defaults = pathjoin(here, 'defaults.yaml')
    config = hiyapyco.load(defaults, path, method=hiyapyco.METHOD_MERGE)

    # The default config contains an example server that needs to be removed
    del config['servers']['defaults.example.com']

    # Supply defaults for server settings
    # This isn't 100% robust but should be good enough.
    for server in config['servers'].values():
        for setting, value in config['server_defaults'].items():
            if setting not in server:
                server[setting] = value
    return config


def load_pkgfile(path):
    """ Load package data file"""

    here = dirname(realpath(__file__))
    path = pathjoin(here, path)
    with open(path) as f:
        return f.read()


def escape_str(s):
    """ Escape control characters in string """
    return s.encode('unicode_escape').decode('utf-8')


def hash_str(alg, s, enc='utf-8'):
    """ Hash a string and return a hexdigest

    Nine times out of ten that's what we actually want.
    """
    return hashlib.new(alg, s.encode(enc)).hexdigest()


def eval_credhash(expr, user, creds):
    """ Eval the credential hash hook

    This handles encoding the attribute values and returns the hexdigest.
    """
    # Created attribute-accessible namespaces
    context = dict()
    context['user'] = SimpleNamespace(**user)
    context['creds'] = SimpleNamespace(**creds)
    for alg in hashlib.algorithms_available:
        # Note: various permutations of lambdas here don't work.
        # See: https://docs.python-guide.org/writing/gotchas/#late-binding-closures
        context[alg] = partial(hash_str, alg)
    try:
        pwhash = eval(expr, context)
    except Exception as ex:
        msg = "Error '%s' while evaluating hash expression: '%s'"
        log.error(msg, ex, expr)
        raise

    # Sanity check
    if pwhash == creds.password:
        msg = "hasher expression '{}' appears to be a no-op?"
        raise ValueError(msg.format(expr))

    return pwhash


# Credential and User are dict-like classes that exist mainly to enforce
# required fields and allow dot lookups. They inherit dict (rather than
# implementing the Mapping methods) because they will be passed to
# pymysql.cursor.execute, which explicitly checks for dict instances rather
# than duck typing.
class Credential(dict):
    """ User-submitted credentials """

    sanitized_pw = "********"

    def __init__(self, user, server, password=None):
        # Not all commands provide a password, so defaults to None
        super().__init__(user=user, server=server, password=password)

    def __str__(self):
        # Overridden so we don't accidentally print a plaintext password
        return str(self.sanitized())

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)

    def sanitized(self):
        """ Get a Credential with the password blotted out

        Useful for debugging sql queries without printing passwords."""
        out = self.copy()
        out['password'] = self.sanitized_pw
        return out


class User(dict):
    """ User DB data collection """

    def __init__(self, name, password, **kwargs):
        super().__init__(name=name, password=password, **kwargs)

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            msg = f"'{type(self).__name__}' has no attribute '{name}'"
            raise AttributeError(msg)


class CommandReader:
    """ Input stream reader

    An iterator. Reads one command at a time.
    """
    def __init__(self, stream):
        self.stream = stream

    def __iter__(self):
        return self

    def __next__(self):
        header = self.stream.read(2)
        if not header:
            raise StopIteration
        sz_payload = int.from_bytes(header, byteorder='big')
        payload = self.stream.read(sz_payload).decode()
        # When splitting the payload, maxsplit is necessary to account for
        # passwords that contain colons.
        cmd, *args = payload.split(":", 3)
        return cmd, Credential(*args)


class ResultWriter:
    """ Output stream writer

    Overkill, but here for symmetry with CommandReader
    """
    def __init__(self, stream):
        self.stream = stream

    def write(self, value):
        """ Write command results to output """
        self.stream.write(struct.pack('!HH', 2, value))
        self.stream.flush()


class Server:
    """ SQL server object

    The guts of the system. Each ejabberd command is implemented as
    cmd_{commandname}.
    """
    def __init__(self, name, connection, hasher=None, queries=None,
                 context=None):
        if hasher is None:
            log.warning("no hash specified for passwords, assuming plaintext")
            hasher = "creds.password"

        self.name = name
        self.db = pymysql.connect(**connection)
        self.queries = SimpleNamespace(**queries)
        self.context = context
        self.hasher = hasher

        self.cursor = self.db.cursor(DictCursor)

    def run(self, command, credential):
        """ Run a command by name """
        try:
            cmd_func = getattr(self, "cmd_" + command)
        except AttributeError:
            raise Exception("received bogus command: " + command)
        return cmd_func(credential)

    def _getuser(self, credential):
        # Format trusted data with .format, then pass untrusted data to
        # execution (which takes %s fmt strings). There has to be a better way
        # to identify the two.
        query = self.queries.user.format(**self.context)
        safe_cred = credential.sanitized()
        query_debug = escape_str(self.cursor.mogrify(query, safe_cred))
        log.debug(f'executing sql query: {query_debug}')
        self.cursor.execute(query, credential)
        result = self.cursor.fetchone()
        return User(**result) if result else None

    def cmd_auth(self, credential):
        """ Authenticate a user """
        user = self._getuser(credential)
        if not user:
            msg = "failed auth for %(user)s: user doesn't exist"
            log.info(msg, credential)
            return False

        pwhash = eval_credhash(self.hasher, user, credential)
        if user.password == pwhash:
            msg = "successful auth for %(user)s"
            log.info(msg, credential)
            return True
        else:
            msg = "failed auth for %s: password mismatch"
            log.info(msg, credential.user)
            # We may want to debug hashers, but we don't want to log plaintext
            # passwords. Only log mismatches if we're reasonably sure the
            # password is hashed.
            if self.hasher and pwhash != credential.password:
                log.debug("(%s != %s)", pwhash, user.password)
            else:
                log.debug("mismatch log suppressed (password is plaintext)")
            return False

    def cmd_isuser(self, credential):
        """ Check if a user exists """
        return self._getuser(credential) is not None

    def cmd_setpass(self, credential):
        """ Set user password """
        raise NotImplementedError

    def cmd_tryregister(self, credential):
        """ Register a user """
        raise NotImplementedError

    def cmd_removeuser(self, credential):
        """ Unconditionally remove a user """
        raise NotImplementedError

    def cmd_removeuser3(self, credential):
        """ Remove a user iff the supplied password is correct """
        raise NotImplementedError


def mainloop(instream, outstream, config):
    """ ejd2mysql main loop

    instream and outstream must be binary streams (typically stdin.buffer and
    stdout.buffer). config should be the object returned from load_config.
    """
    reader = CommandReader(instream)
    writer = ResultWriter(outstream)
    servers = {name: Server(name, **conf)
               for name, conf
               in config['servers'].items()}

    log.info("starting main loop")
    i = 0
    for i, (cmd, credential) in enumerate(reader, 1):
        assert cmd
        try:
            writer.write(servers[credential.server].run(cmd, credential))
        except NotImplementedError:
            log.warning("%s received but not implemented, rejecting", cmd)
            writer.write(False)
    log.info("end of input, processed %s commands", i)


def main(argv=None):
    """ Execute ejd2mysql

    If argv is not supplied, defaults to sys.argv[1:]
    """
    if argv is None:
        argv = sys.argv[1:]

    desc = "external script for ejabberd->mysql authentication"
    parser = ArgumentParser(description=desc)
    parser.add_argument('conf_file', nargs='?',
                        help="configuration file path")
    parser.add_argument('-V', '--version', action='store_true',
                        help="print version and exit")
    parser.add_argument('-v', '--verbose', action='store_true',
                        help="verbose output")
    parser.add_argument('-D', '--debug', action='store_true',
                        help="even more verbose output")
    parser.add_argument('--confdbg', action='store_true',
                        help="dump effective configuration")
    args = parser.parse_args(argv)

    # Print version if requested
    if args.version:
        print(version)
        sys.exit(0)

    # Load config, dump if requested
    if args.conf_file is None:
        args.conf_file = "/etc/ejabberd/ejd2mysql.yaml"
    config = load_config(args.conf_file)

    if args.confdbg:
        print(hiyapyco.dump(config, default_flow_style=False))
        sys.exit(0)

    # Set up logging
    logging.config.dictConfig(config['logging'])
    if args.debug:
        cli_loglevel = logging.DEBUG
    elif args.verbose:
        cli_loglevel = logging.INFO
    else:
        cli_loglevel = logging.WARN
    if cli_loglevel < log.getEffectiveLevel():
        log.setLevel(cli_loglevel)
    log.info("starting ejd2mysql v%s", version)
    log.info("verbose output enabled")
    log.debug("debug output enabled")

    # Go
    try:
        mainloop(sys.stdin.buffer, sys.stdout.buffer, config)
    except Exception as ex:
        log.critical("unhandled exception: %s", ex)
        log.exception(ex)
        raise


if __name__ == "__main__":
    main()
