# ejd2mysql

An ejabberd external auth script for MySQL

ejd2mysql allows ejabberd to authenticate against a MySQL database owned
by a different application. The motivating case was a web forum; I
wanted users to be able to log into ejabberd using their forum
credentials.

Another such script [exists already][eam], but it did not suit me. I
prefer config files to environment variables, and its password-hashing
options were not flexible enough. So I wrote my own. It may be useful to
others.

**At the moment, ejd2mysql can only authenticate users; it can't
register new ones.** That is probably fine, since the intended use case
is authenticating against an external application's database. Presumably
that application will be how users register. But if someone else wants
to implement this, I do take PRs.

## Overview and Installation

Here's what you'll be doing, short-form:

1. Install ejd2mysql
2. Configure ejd2mysql to connect to your database and find user data
3. Configure ejabberd to use ejd2mysql as an authentication backend

ejd2mysql's only system dependency is Python 3, and it can be installed
from pip. All example commands and paths are for an Ubuntu Linux system;
other operating systems will differ.

```sh
apt update
apt install python3-pip
pip3 install ejd2mysql
```

## Configuring ejd2mysql

By default, `ejd2mysql` looks for its configuration file at
`/etc/ejabberd/ejd2mysql.yaml`, although you can specify another as a
command-line argument. The file is in YAML format, and should look like
this:

```yaml
servers:
  xmpp.example.com:
    # The connection block is the only mandatory part
    connection:
      host: localhost
      user: ejabberd
      password: notarealpassword
      db: dbname
    # `hasher` defines how DB passwords are hashed, if they are
    hasher: sha1(creds.password + user.salt)

    # queries are optional-ish; more on them in the next section.
    context:
      table: ejd_users
    queries:
      user: |
        select * from {table}
        where lower(name) = lower(%(user)s)
```

`connection` specifies the settings ejd2mysql should use to connect to
the external database. The options listed are the most common. Under the
hood these are passed to [pymysql.connect()][pms], so any of its keyword
arguments should work.

`hasher` specifies the method by which passwords are hashed. This must
match the way your external application stores passwords. The value is
evaluated as a Python expression. The submitted credentials are
available as `creds.user` and `creds.password`. The corresponding
database values are available as `user.name` and `user.password`.
Additional attributes may be available on the user object; more on that
below.

Within the hasher expression, string-to-hexdigest hash functions are
available for most hashing algorithms. The example uses sha1, but
anything in [hashlib.algorithms_available][algs] should work.

`queries` and `context` are used to look up user data. See the next
section.

## Locating User Data

To authenticate users, ejd2mysql needs to know how to find them in the
database. By default, it looks in the `ejd_users` table for an entry
with a `name` matching the user we are authing. This almost certainly
won't work out of the box, since the external application may keep user
data anywhere.

There are two ways to deal with this. The recommended way is to create a
database view that maps the application's tables and columns into the
form that `ejd2mysql` expects. If that is not possible -- perhaps
because your access to the database is read-only -- then you must
provide a sql query to perform user lookups.

### View-Based Mapping

By default, ejd2mysql looks for a table or view named `ejd_users`; you
can change this with the `context:table` option.

That view must include, at a minimum, `name` and `password` columns,
plus any attributes needed by the `hasher` expression. The user we are
authenticating will be matched against the `name` column (case
insensitive). If found, their password will be hashed using the `hasher`
expression, then compared to the `password` column.

In the following example, user credentials are stored in one table, a
list of banned users are stored in another, and password hashes use a
per-user salt:

```sql
create or replace view ejd_users as
select
    ut.username as name,
    ut.passwordhash as password,
    ut.hashing_salt as salt,
from
    user_table as ut
    left join banned_users as bans
        on ut.userid = bans.userid
where
    ut.enabled != 0
    and bans.userid is NULL
```

This view collects non-banned users and returns their name, password,
and salt. The default query can then be applied.

### Custom Queries

If you can't create a view on the database side, you can instead provide
a custom query in `ejd2mysql.yaml`. It should return a single user. As
with a view, the query result should include a name, password, and any
additional attributes needed.

Custom queries can reference credential variables (specifically server,
user, and password) with `%(varname)s`. They will be quoted and escaped
as necessary.

Subkeys of the server's `context` key are also available, and can be
referenced as `{varname}`. Context values are inserted verbatim. This
is intended to support substituting a table name into the default query.
It is unlikely to be necessary when customizing, since you can include
the correct table name directly in the query.

The following example is equivalent to the view above:

```yaml
servers:
  xmpp.example.com:
    context:
      table: user_table
    queries:
      user: |
        select
            ut.username     as name,
            ut.passwordhash as password,
            ut.hashing_salt as salt,
        from
            {table} as ut
            left join banned_users as bans
                on ut.userid = bans.userid
        where
            ut.enabled != 0
            and bans.userid is NULL
            and lower(name) = lower(%(user)s)
```

Note the final comparison.

The custom query method clutters the config file, but sometimes nothing
else will do.

## Configuring ejabberd

The relevant settings are under `host_config` and should look like this:

```yaml
host_config:
  "xmpp.example.com":
    auth_method: [external]
    extauth_program: "/usr/local/bin/ejd2mysql"
```

Older versions of ejabberd do not quite conform to the yaml standard
when interpreting its config file. In particular, its interpretation of
quoted strings was idiosyncratic. Use quotes exactly as shown above. This
issue may be fixed in ejabberd >= 19.08.

## Troubleshooting

### Extra Debugging Information

There are several options to aid debugging; you can get a list with
`ejd2mysql --help`. The `--verbose` and `--debug` flags will raise the
loglevel to INFO and DEBUG respectively (the default is WARN).
`--confdbg` prints out the "effective configuration"; that is, the
configuration `ejd2mysql` sees after loading the defaults and the config
file.

While not described above, there is a `logging` configuration key. It
follows the [dictConfig schema][logdc], and is rather complex, but it's
there if you need more control.

All defaults are documented in [defaults.yaml](src/defaults.yaml).

### YAML quirks

YAML is easy to read but sometimes hard to write correctly. One of the
most common stumbling points is multiline strings. ejd2mysql's conf file
can usually get away without them...except for custom sql queries. And
those are already the most complicated thing you can do here.

In short, to get a multiline string: start with a pipe character, then
indent everything under it at least two spaces past the key. See the
example above.

This [Brief YAML Reference][yaml] is a pretty good guide for more.

### Authentications not logged

`ejabberd` caches authentication data. If it already "knows" a given
user's password is correct, you won't see an entry in ejd2mysql.log.
This may be what you want in production, but it can interfere with
testing.

The cache setting is in ejabberd.yml. You can turn it off like so:

```yaml
host_config:
  "xmpp.example.com":
    auth_method: [external]
    extauth_program: "/usr/local/bin/ejd2mysql"
    use_cache: false    # <----add this line here
```

### "External authentication program failed" with no log entry

This seems to happen if ejd2mysql is upgraded or reinstalled but
ejabberd is not restarted. Restart ejabberd.

[eam]: https://github.com/rankenstein/ejabberd-auth-mysql
[pms]: https://pymysql.readthedocs.io/en/latest/modules/connections.html
[algs]: https://docs.python.org/3/library/hashlib.html#hashlib.algorithms_available
[yaml]: https://camel.readthedocs.io/en/latest/yamlref.html
[logdc]: https://docs.python.org/3/library/logging.config.html#logging-config-dictschema
